# WishSimply Wordpress plugin

WishSimply Wordpress plugin development happens here. Once a new version is ready it will be committed to wordpress.org svn repository at http://plugins.svn.wordpress.org/wishsimply/trunk/

So that it is available for all the WordPress users easily. 

WishSimply is a free online wish service list that seriously respects privacy. 
No ads, facebook connect, google analytics or 3rd party email senders with trackers. 
Read more in [WishSimply's privacy policy](https://wishsimply.com/privacy-policy) 

This plugin will help user to publish and manage their own wishlists on their own blog.

See more in few slides that how to make a [public wishsimply wish list](https://www.slideserve.com/WishSimply/how-to-use-public-lists-at-wishsimply) 
or read more from [WishSimply blog](https://wishsimply.com/blog/fi/public-wish-list-in-wishsimply/).



--

FI: 
WishSimply on yksityisyyttä kunniottava [lahjalista palvelu](https://wishsimply.com/fi).

WishSimplyn WordPress liitännäisen kehitys tapahtuu täällä, josta se se sitten lähetetään
wordpress.org:in ylläpitämään järjestelmään, josta se on saatavilla kaikille WordPress palvelimille.
